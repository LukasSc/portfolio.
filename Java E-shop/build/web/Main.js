
$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getUserName", dataType: "text", method: "GET"});
    request.done(response => {
        document.getElementById("name").innerHTML = response;
        if (response != "") {
            $("#logout").show();
            $("#login").hide();
        } else {
            $("#logout").hide();
            $("#login").show();
        }
    });
    request.fail(response => {

        $("#logout").hide();
        $("#login").show();
    });
});


$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getAdmin", dataType: "text", method: "GET"});
    request.done(response => {
        if (response == 1) {
            $("#admin").show();
        } else {
            $("#admin").hide();
        }

    });
    request.fail(response => {

        $("#admin").hide();

    });
});



$("#logout").click(function () {
    request = $.ajax({url: "rest/ninjatools/killsession", method: "POST", contentType: "application/json; charset=utf-8", dataType: "json"});
    request.done(response => {
        $("#name").hide();
        $("#logout").hide();
        $("#admin").hide();
        $("#login").show();
        window.location.href = "/NinjaCode";
        document.getElementById("quant").innerHTML = 0;

    });
});


$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getGoods", method: "GET"});
    request.done(response => {
        $("#tbody").empty();
        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td>" + response[i].name + "</td>";
            newTr += "<td>" + response[i].quantity + "</td>";
            newTr += "<td>" + response[i].price + "</td>";
            newTr += "<td>" + response[i].description + "</td>";
            newTr += "<td><input id=\"quantity" + response[i].id + "\" type=\"number\" value=\"1\" min=\"1\" max=" + response[i].quantity + "><button  class=\"addtocart\" value=" + response[i].id + ">Add to Cart</button></td>";
            newTr += "</tr>";

            $("#tbody").append(newTr);
        }

        $(".addtocart").click(function (e) {

            let goodId = e.currentTarget.value;
            let quantity = $("#quantity" + goodId).val();
            if (parseInt(quantity) >= 1 && parseInt(quantity) <= parseInt($('#quantity' + goodId).attr('max'))) {
                let goodQuantity = {
                    quantity: quantity
                };
                request = $.ajax({url: "rest/ninjatools/addtocart?goodId=" + goodId, method: "POST", data: JSON.stringify(goodQuantity), contentType: "application/json; charset=utf-8", dataType: "json"});
                request.done(response => {
                    document.getElementById("quant").innerHTML = response;
                   
                });

            } else {
                alert("Wrong quantity!");
            }
        });
    });

});


$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getProductsQuantity", dataType: "text", method: "GET"});
    request.done(response => {
        document.getElementById("quant").innerHTML = response;


    });
});


$("#toCart").click(function () {
    window.location.href = "Cart.html";
});



