package lt.ninja.classes;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cart.class)
public abstract class Cart_ {

	public static volatile SingularAttribute<Cart, Date> purchaseDate;
	public static volatile SingularAttribute<Cart, Integer> id;
	public static volatile SingularAttribute<Cart, Users> userId;
	public static volatile SingularAttribute<Cart, Date> createDate;

	public static final String PURCHASE_DATE = "purchaseDate";
	public static final String ID = "id";
	public static final String USER_ID = "userId";
	public static final String CREATE_DATE = "createDate";

}

