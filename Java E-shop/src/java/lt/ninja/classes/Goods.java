/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lukas
 */
@Entity
@Table(name = "goods")
@NamedQueries({
    @NamedQuery(name = "Goods.findAll", query = "SELECT g FROM Goods g")
    , @NamedQuery(name = "Goods.findById", query = "SELECT g FROM Goods g WHERE g.id = :id")
    , @NamedQuery(name = "Goods.findByName", query = "SELECT g FROM Goods g WHERE g.name = :name")
    , @NamedQuery(name = "Goods.findByQuantity", query = "SELECT g FROM Goods g WHERE g.quantity = :quantity")
    , @NamedQuery(name = "Goods.findByPrice", query = "SELECT g FROM Goods g WHERE g.price = :price")
    , @NamedQuery(name = "Goods.findByDescription", query = "SELECT g FROM Goods g WHERE g.description = :description")})
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private int quantity;

    private BigDecimal price;

    private String description;

    private List<Receipsrows> receipsrowsList;

    private List<Cartrows> cartrowsList;

    public Goods() {
    }

    public Goods(Integer id) {
        this.id = id;
    }

    public Goods(Integer id, String name, int quantity, BigDecimal price, String description) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "goodId")
    public List<Receipsrows> getReceipsrowsList() {
        return receipsrowsList;
    }

    public void setReceipsrowsList(List<Receipsrows> receipsrowsList) {
        this.receipsrowsList = receipsrowsList;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "goodId")
    public List<Cartrows> getCartrowsList() {
        return cartrowsList;
    }

    public void setCartrowsList(List<Cartrows> cartrowsList) {
        this.cartrowsList = cartrowsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Goods)) {
            return false;
        }
        Goods other = (Goods) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.ninja.classes.Goods[ id=" + id + " ]";
    }

}
