/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lukas
 */
@Entity
@Table(name = "cartrows")
@NamedQueries({
    @NamedQuery(name = "Cartrows.findAll", query = "SELECT c FROM Cartrows c")
    , @NamedQuery(name = "Cartrows.findById", query = "SELECT c FROM Cartrows c WHERE c.id = :id")
    , @NamedQuery(name = "Cartrows.findByQuantity", query = "SELECT c FROM Cartrows c WHERE c.quantity = :quantity")
    , @NamedQuery(name = "Cartrows.findByPrice", query = "SELECT c FROM Cartrows c WHERE c.price = :price")
    ,@NamedQuery(name = "Cartrows.findByCartId", query = "SELECT c FROM Cartrows c where c.cartId = :cartId")})
public class Cartrows implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private int quantity;

    private BigDecimal price;

    private Cart cartId;

    private Goods goodId;

    public Cartrows() {
    }

    public Cartrows(Integer id) {
        this.id = id;
    }

    public Cartrows(Integer id, int quantity, BigDecimal price) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JoinColumn(name = "cartId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Cart getCartId() {
        return cartId;
    }

    public void setCartId(Cart cartId) {
        this.cartId = cartId;
    }

    @JoinColumn(name = "goodId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Goods getGoodId() {
        return goodId;
    }

    public void setGoodId(Goods goodId) {
        this.goodId = goodId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cartrows)) {
            return false;
        }
        Cartrows other = (Cartrows) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.ninja.classes.Cartrows[ id=" + id + " ]";
    }

}
