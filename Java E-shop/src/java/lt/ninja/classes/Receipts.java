/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lukas
 */
@Entity
@Table(name = "receipts")
@NamedQueries({
    @NamedQuery(name = "Receipts.findAll", query = "SELECT r FROM Receipts r")
    , @NamedQuery(name = "Receipts.findById", query = "SELECT r FROM Receipts r WHERE r.id = :id")
    , @NamedQuery(name = "Receipts.findByReceipDate", query = "SELECT r FROM Receipts r WHERE r.receipDate = :receipDate")
    , @NamedQuery(name = "Receipts.findByInfo", query = "SELECT r FROM Receipts r WHERE r.info = :info")})
public class Receipts implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Date receipDate;

    private String info;

    private List<Receipsrows> receipsrowsList;

    public Receipts() {
    }

    public Receipts(Integer id) {
        this.id = id;
    }

    public Receipts(Integer id, Date receipDate) {
        this.id = id;
        this.receipDate = receipDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "receipDate")
    @Temporal(TemporalType.DATE)
    public Date getReceipDate() {
        return receipDate;
    }

    public void setReceipDate(Date receipDate) {
        this.receipDate = receipDate;
    }

    @Size(max = 45)
    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receipId")
    public List<Receipsrows> getReceipsrowsList() {
        return receipsrowsList;
    }

    public void setReceipsrowsList(List<Receipsrows> receipsrowsList) {
        this.receipsrowsList = receipsrowsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Receipts)) {
            return false;
        }
        Receipts other = (Receipts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.ninja.classes.Receipts[ id=" + id + " ]";
    }

}
