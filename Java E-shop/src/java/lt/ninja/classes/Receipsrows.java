/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lukas
 */
@Entity
@Table(name = "receipsrows")
@NamedQueries({
    @NamedQuery(name = "Receipsrows.findAll", query = "SELECT r FROM Receipsrows r")
    , @NamedQuery(name = "Receipsrows.findById", query = "SELECT r FROM Receipsrows r WHERE r.id = :id")
    , @NamedQuery(name = "Receipsrows.findByQuantity", query = "SELECT r FROM Receipsrows r WHERE r.quantity = :quantity")
    , @NamedQuery(name = "Receipsrows.findByGoodId", query = "SELECT r FROM Receipsrows r WHERE r.goodId = :goodId")})
public class Receipsrows implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private int quantity;

    private Goods goodId;

    private Receipts receipId;

    public Receipsrows() {
    }

    public Receipsrows(Integer id) {
        this.id = id;
    }

    public Receipsrows(Integer id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @JoinColumn(name = "goodId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Goods getGoodId() {
        return goodId;
    }

    public void setGoodId(Goods goodId) {
        this.goodId = goodId;
    }

    @JoinColumn(name = "receipId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Receipts getReceipId() {
        return receipId;
    }

    public void setReceipId(Receipts receipId) {
        this.receipId = receipId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Receipsrows)) {
            return false;
        }
        Receipsrows other = (Receipsrows) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.ninja.classes.Receipsrows[ id=" + id + " ]";
    }

}
