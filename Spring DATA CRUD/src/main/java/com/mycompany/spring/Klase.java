/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;
import repositories.AddressesRep;
import repositories.ContactsRep;
import repositories.PeopleRep;

/**
 *
 * @author Lukas
 */
@Controller
@RequestMapping

public class Klase {

    @Autowired
    private PeopleRep personObj;
    @Autowired
    private AddressesRep adresai;
    @Autowired
    private ContactsRep kontaktai;

    @RequestMapping
    public ModelAndView kazkas(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("peopleList");
        //mav.addObject("list", kepalas.findByName("%as"));
        // mav.addObject("list", kepalas.findByLastName("Ostapenko"));
        mav.addObject("list", personObj.findAll());

        return mav;
    }

    @RequestMapping(path = "getAddresses/{id}", method = RequestMethod.GET)
    public ModelAndView getAddress(@PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("addresses");
        People fp = personObj.getOne(id);
        List<Addresses> pplAddress = adresai.findByPeopleId(fp);
        mav.addObject("list", pplAddress);
        mav.addObject("pplid", id);

        return mav;

    }

    @RequestMapping(path = "getContacts/{id}", method = RequestMethod.GET)
    public ModelAndView getContacts(@PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("contacts");
        People fp = personObj.getOne(id);
        List<Contacts> pplContacts = kontaktai.findByPeopleId(fp);
        mav.addObject("list", pplContacts);
        mav.addObject("pplid", id);
        return mav;

    }

    @Transactional
    @RequestMapping(path = "editPerson/{id}", method = RequestMethod.GET)
    public ModelAndView editPerson(@PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("editPerson");
        if ((!id.equals(0))) {
            People foundppl = personObj.getOne(id);
            foundppl.setAddressesList(null);
            foundppl.setContactsList(null);

            mav.addObject("person", foundppl);

            return mav;
        } else {
            People newppl = new People();
            newppl.setAddressesList(null);
            newppl.setContactsList(null);
            newppl.setFirstName(null);
            newppl.setLastName(null);
            newppl.setBirthDate(null);
            newppl.setSalary(null);
            newppl.setId(0);

            mav.addObject("person", newppl);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "savePeople/{id}", method = RequestMethod.POST)
    public ModelAndView changePerson(@RequestParam(value = "firstName", required = true) String firstName,
            @RequestParam(value = "lastName", required = true) String lastName,
            @RequestParam(value = "birthDate", required = true) String birthDate,
            @RequestParam(value = "salary", required = true) BigDecimal salary,
            @PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/");
        if ((!id.equals(0))) {
            People foundPerson = personObj.getOne(id);
            foundPerson.setAddressesList(null);
            foundPerson.setContactsList(null);
            foundPerson.setFirstName(firstName);
            foundPerson.setLastName(lastName);
            foundPerson.setBirthDate(Date.valueOf(birthDate));
            foundPerson.setSalary(salary);
            personObj.save(foundPerson);

            return mav;
        } else {
            People newppl = new People();
            newppl.setAddressesList(null);
            newppl.setContactsList(null);
            newppl.setFirstName(firstName);
            newppl.setLastName(lastName);
            newppl.setBirthDate(Date.valueOf(birthDate));
            newppl.setSalary(salary);

            personObj.save(newppl);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "deletePerson/{id}", method = RequestMethod.GET)
    public ModelAndView deletePerson(Model model, @PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/");
        People foundPerson = personObj.getOne(id);
        foundPerson.setAddressesList(null);
        foundPerson.setContactsList(null);

        personObj.delete(foundPerson);
        return mav;
    }

    @Transactional
    @RequestMapping(path = "getAddresses/editAddress/{id}/{pplid}", method = RequestMethod.GET)
    public ModelAndView editAddress(@PathVariable("id") Integer id, @PathVariable("pplid") Integer pplid) {
        ModelAndView mav = new ModelAndView("editAddress");
        if ((!id.equals(0))) {
            Addresses foundadd = adresai.getOne(id);

            mav.addObject("address", foundadd);
            return mav;

        } else {
            Addresses newadd = new Addresses();
            newadd.setAddress(null);
            newadd.setCity(null);
            newadd.setPc(null);
            newadd.setId(0);

            mav.addObject("address", newadd);
            mav.addObject("pplid", pplid);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "saveAddress/{id}/{pplid}", method = RequestMethod.POST)
    public ModelAndView saveAddress(
            @RequestParam(value = "address", required = true) String address,
            @RequestParam(value = "city", required = true) String city,
            @RequestParam(value = "pc", required = true) String pc,
            @PathVariable("id") Integer id,
            @PathVariable("pplid") Integer pplid) {
        ModelAndView mav = new ModelAndView("redirect:/getAddresses/{pplid}");
        if ((!id.equals(0))) {
            Addresses foundAddress = adresai.getOne(id);

            foundAddress.setAddress(address);
            foundAddress.setCity(city);
            foundAddress.setPc(pc);

            adresai.save(foundAddress);

            return mav;
        } else {
            People foundppl = personObj.getOne(pplid);
            Addresses newadd = new Addresses();

            newadd.setAddress(address);
            newadd.setCity(city);

            newadd.setPc(pc);
            newadd.setPeopleId(foundppl);

            adresai.save(newadd);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "getAddresses/deleteAddress/{id}/{pplid}", method = RequestMethod.GET)
    public ModelAndView deleteAddress(Model model, @PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/getAddresses/{pplid}");
        Addresses foundadd = adresai.getOne(id);

        adresai.delete(foundadd);
        return mav;
    }

    @Transactional
    @RequestMapping(path = "getContacts/editContact/{id}/{pplid}", method = RequestMethod.GET)
    public ModelAndView editContact(@PathVariable("id") Integer id, @PathVariable("pplid") Integer pplid) {
        ModelAndView mav = new ModelAndView("editContact");
        if ((!id.equals(0))) {
            Contacts foundcon = kontaktai.getOne(id);

            mav.addObject("contact", foundcon);
            return mav;

        } else {
            Contacts newcon = new Contacts();
            newcon.setCtype(null);
            newcon.setContact(null);

            newcon.setId(0);

            mav.addObject("contact", newcon);
            mav.addObject("pplid", pplid);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "saveContact/{id}/{pplid}", method = RequestMethod.POST)
    public ModelAndView saveContact(
            @RequestParam(value = "ctype", required = true) String ctype,
            @RequestParam(value = "contact", required = true) String contact,
            @PathVariable("id") Integer id,
            @PathVariable("pplid") Integer pplid) {
        ModelAndView mav = new ModelAndView("redirect:/getContacts/{pplid}");
        if ((!id.equals(0))) {
            Contacts foundContact = kontaktai.getOne(id);

            foundContact.setCtype(ctype);
            foundContact.setContact(contact);

            kontaktai.save(foundContact);

            return mav;
        } else {
            People foundppl = personObj.getOne(pplid);
            Contacts newcon = new Contacts();

            newcon.setCtype(ctype);
            newcon.setContact(contact);

            newcon.setPeopleId(foundppl);

            kontaktai.save(newcon);

            return mav;
        }
    }

    @Transactional
    @RequestMapping(path = "getContacts/deleteContact/{id}/{pplid}", method = RequestMethod.GET)
    public ModelAndView deleteContact(Model model, @PathVariable("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/getContacts/{pplid}");
        Contacts foundcon = kontaktai.getOne(id);

        kontaktai.delete(foundcon);
        return mav;
    }
}
