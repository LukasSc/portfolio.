<%-- 
    Document   : puslapis1
    Created on : Oct 18, 2018, 5:31:29 PM
    Author     : Lukas
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style>
            table{
             
                width:100%;
            }
             td{
              
                text-align: center;
               
            }
             th{
                text-align: center;
                background: lightblue;
            }
            button{
                width:100%
            }
             button:hover{
                cursor:pointer;
            }
            table tr:hover td{
                background:lightseagreen;
            }
            .margin{
                margin-top: 1vw;
                width:100%;
            }
              .green{
                background: lightseagreen;
            }
             .blue{
                background: lightgreen;
            }
             .red{
                background: red;
            }
            .orange{
                background: yellow;
            }
        </style>
    </head>
    <body>
      <div class="container margin">
        <table class="table table-bordred table-striped">
            <thead>
            <th>Name</th>
            <th>Surname</th>
            <th>Birth date</th>
            <th>Salary</th>
            <th><a href="editPerson/0"><button class="green btn"> Add new </button></a></th>
            <th></th>
            <th></th>
            <th></th>
            </thead>
            <c:forEach var="People" items="${list}">
                <tr>
                    <td>${People.firstName}</td>
                    <td>${People.lastName}</td>
                    <td>${People.birthDate}</td>
                    <td>${People.salary}</td>
                     <td><a href="getAddresses/${People.id}"><button class="orange btn">Addresses</button></a></td>
                    <td><a href="getContacts/${People.id}"><button class="orange btn">Contacts</button></a></td>
                    <td><a href="editPerson/${People.id}"><button class="blue btn">Edit</button></a></td>
                    <td><a href="deletePerson/${People.id}"><button class="red btn">Delete</button></a></td>
                    
                </tr>
            </c:forEach>
        </table>
      </div>
    </body>
</html>
