<%-- 
    Document   : contacts
    Created on : Oct 26, 2018, 2:41:40 PM
    Author     : Lukas
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style>
            table{
              
                width:100%;
            }
            td{
            
                text-align: center;
               
            }
            th{
                text-align: center;
                background: lightblue;
            }
            button{
                width:100%;
            }
            button:hover{
                cursor:pointer;
            }
            table tr:hover td{
                background:lightseagreen;
            }
            .margin{
                margin-top: 1vw;
                width:100%;
            }
              .green{
                background: lightseagreen;
            }
             .blue{
                background: lightgreen;
            }
             .red{
                background: red;
            }
             
        </style>
    </head>
    <body>
        <div class="container margin">
            <table class="table table-bordred table-striped">
                <thead>
                <th>C-type</th>
                <th>Contact</th>


                <th><a href="/Spring"><button class="blue btn">Back</button></a></th>
                <th><a href="editContact/0/${pplid}"><button class="green btn">Add new</button></a></th>
                </thead>
                <c:forEach var="Contacts" items="${list}">
                    <tr>
                        <td>${Contacts.ctype}</td>
                        <td>${Contacts.contact}</td>

                        <td><a href="editContact/${Contacts.id}/${pplid}"><button class="blue btn">Edit</button></a></td>
                        <td><a href="deleteContact/${Contacts.id}/${pplid}"><button class="red btn">Delete</button></a></td>

                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
